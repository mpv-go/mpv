FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > mpv.log'

COPY mpv .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' mpv
RUN bash ./docker.sh

RUN rm --force --recursive mpv
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD mpv
